package hu.icell.hr.manager.dao;

import java.io.Serializable;
import java.util.List;

import hu.icell.hr.manager.model.BusinessObject;

public interface BaseDAO<T extends BusinessObject, ID extends Serializable> {

	void persist(T object);
	
	void update(T object);
	
	void delete(T object);
	
	List<T> getAll();
	
	T getById(ID id);
}
