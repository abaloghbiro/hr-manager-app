package hu.icell.hr.manager.dao;

import hu.icell.hr.manager.model.Employee;

public interface EmployeeDAO extends BaseDAO<Employee, Long> {

}
