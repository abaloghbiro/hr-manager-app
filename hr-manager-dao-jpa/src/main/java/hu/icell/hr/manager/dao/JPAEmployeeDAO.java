package hu.icell.hr.manager.dao;import javax.enterprise.context.ApplicationScoped;

import hu.icell.hr.manager.model.Employee;

@ApplicationScoped
public class JPAEmployeeDAO extends JpaDAO<Employee, Long> implements EmployeeDAO {

}
