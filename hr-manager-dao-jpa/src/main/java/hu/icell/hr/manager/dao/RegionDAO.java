package hu.icell.hr.manager.dao;import javax.enterprise.context.ApplicationScoped;

import hu.icell.hr.manager.model.Region;

@ApplicationScoped
public class RegionDAO extends JpaDAO<Region, Long> {

}
