package hu.icell.hr.manager.dao;import javax.enterprise.context.ApplicationScoped;

import hu.icell.hr.manager.model.JobHistory;
import hu.icell.hr.manager.model.JobHistoryPK;

@ApplicationScoped
public class JobHistoryDAO extends JpaDAO<JobHistory, JobHistoryPK> {

}
