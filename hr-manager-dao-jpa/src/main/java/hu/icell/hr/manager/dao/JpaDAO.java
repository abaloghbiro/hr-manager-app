package hu.icell.hr.manager.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.core.GenericTypeResolver;

import hu.icell.hr.manager.model.BusinessObject;

public abstract class JpaDAO<T extends BusinessObject, ID extends Serializable> implements BaseDAO<T, ID> {

	@PersistenceContext(name = "HR")
	protected EntityManager em;

	protected Class<T> clazz;
	
	protected T obj;

	@SuppressWarnings("unchecked")
	public JpaDAO() {
		Class<?>[] clazzes = GenericTypeResolver.resolveTypeArguments(getClass(), JpaDAO.class);
		
		for (Class<?> c : clazzes) {

			if (BusinessObject.class.isAssignableFrom(c)) {
				clazz = (Class<T>) c;
			}
		}

		if (clazz == null) {
			throw new IllegalArgumentException("Couldn't determine business object current type!");
		}

	}

	public void persist(T object) {
		em.persist(object);

	}

	public void update(T object) {
		em.merge(object);

	}

	public void delete(T object) {
		em.remove(object);

	}

	public List<T> getAll() {
		return em.createQuery("Select e from " + clazz.getSimpleName() + " e", clazz).getResultList();
	}

	public T getById(ID id) {
		return em.find(clazz, id);
	}
}
