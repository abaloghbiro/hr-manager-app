package hu.icell.hr.manager.dao;import javax.enterprise.context.ApplicationScoped;

import hu.icell.hr.manager.model.Location;

@ApplicationScoped
public class LocationDAO extends JpaDAO<Location, Long> {

}
